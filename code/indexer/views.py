from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from clustering.kmeans import KMeans
from ir_engine import SearchEngine
import index
import search_engine
import time

try:
    engine = SearchEngine()
except:
    print("No index found")

@api_view(['POST'])
def process_query(request: Request):
    inp = time.time()
    global engine
    result = search_engine.do_search(engine, request.data)
    outp = time.time()
    print(inp, outp, outp - inp)
    return Response(result)


@api_view(['POST'])
def initialize_indices(request: Request):
    global engine
    index.indexing()
    engine = SearchEngine()
    return Response()


@api_view(['GET'])
def get_doc(request: Request):
    doc_id = request.query_params['id']
    return Response(index.fetch_result([int(doc_id)]))


@api_view(['POST'])
def construct_clusters(request: Request):
    cls = KMeans.create()
