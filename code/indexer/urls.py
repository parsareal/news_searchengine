from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from indexer import views

urlpatterns = [
    path('/process', views.process_query),
    path('/setup', views.initialize_indices),
    path('/doc', views.get_doc),
    path('/cluster', views.construct_clusters)
]

urlpatterns = format_suffix_patterns(urlpatterns)
