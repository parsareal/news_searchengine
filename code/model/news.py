from mongoengine import Document, StringField, IntField, ListField


class NewsModel(Document):
    news_id: IntField(default=0)
    content: StringField(default='')
    title: StringField(default='')
    thumbnail: StringField(default='')
    meta_tags: ListField(StringField(), default=list)
    publish_date: StringField(default='')
    url: StringField(default='')
    summary: StringField(default='')